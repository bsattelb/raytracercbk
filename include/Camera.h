#pragma once

#include <eigen3/Eigen/Dense>
#include <math.h>
#include "Ray.h"
#include "RandomHelper.h"

#include <iostream>

#define PI 3.14159265

class Camera {
public:
  Eigen::Vector3d lowerLeft, horizontal, vertical, origin;
  Eigen::Vector3d u, v, w;
  double lens_radius;
  Camera() {}

  void setUp(Eigen::Vector3d lookfrom, Eigen::Vector3d lookat, Eigen::Vector3d vup, float vfov, float aspect, double aperture, double focus_dist) {
    lens_radius = aperture/2;

    float theta = vfov*PI/180;
    float half_height = std::tan(theta/2);
    float half_width = aspect*half_height;

    origin = lookfrom;

    w = (lookfrom - lookat).normalized();
    u = vup.cross(w).normalized();
    v = w.cross(u);

    //lowerLeft = Eigen::Vector3d(-half_width, -half_height, -1.0);
    lowerLeft = origin - half_width*focus_dist*u - half_height*focus_dist*v - focus_dist*w;
    horizontal = 2*half_width*focus_dist*u;
    vertical = 2*half_height*focus_dist*v;
  }

  Ray getPixelRay(double s, double t) {
    Eigen::Vector3d rd = lens_radius*RandomHelper::randomInUnitDisk();
    Eigen::Vector3d offset = u*rd(0) + v*rd(1);
    return Ray(origin + offset, lowerLeft + s*horizontal + t*vertical - origin - offset);;
  }

  void getRandomOnImagePlane(Eigen::Vector3d &point, Eigen::Vector2d &coords) {
    double s = RandomHelper::randomUniform();
    double t = RandomHelper::randomUniform();
    coords = Eigen::Vector2d(s, t);
    point = lowerLeft + s*horizontal + t*vertical + origin;
    return;
  }
};
