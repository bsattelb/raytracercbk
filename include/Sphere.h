#pragma once
#include <eigen3/Eigen/Dense>

#include "Hitable.h"
#include "BoundingBox.h"


class Sphere: public Hitable {
public:
  Eigen::Vector3d center;
  double radius;

  Sphere(Eigen::Vector3d c, double r, Material *mat) {
    center = c;
    radius = r;
    matPtr = mat;
  }

  bool getTvals(const Ray& r, Eigen::Vector2d& tVals) const;
  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const override;
  virtual bool bounding_box(BoundingBox& box) const override;
  virtual Ray random_ray_from_surface() const override;
  virtual double distanceFrom(const Eigen::Vector3d& point) const;

  virtual double pdf_value(const Eigen::Vector3d& origin, const Eigen::Vector3d v) const override;
  virtual Eigen::Vector3d random(const Eigen::Vector3d& origin) const override;

  virtual bool inside(const Eigen::Vector3d point) const override {
    return (point-center).norm() < radius;
  }

  virtual void updatePosition(Eigen::Vector3d move) {
    center += move;
  }
};
