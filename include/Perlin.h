#pragma once

#include <eigen3/Eigen/Dense>
#include <math.h>

#include "RandomHelper.h"

inline double trilinear_interp(Eigen::Vector3d c[2][2][2], double u, double v, double w) {
  double uu = u*u*(3 -2*u);
  double vv = v*v*(3 -2*v);
  double ww = w*w*(3 -2*w);
  double accum = 0;
  for(int i = 0; i < 2; i++) {
    for(int j = 0; j < 2; j++) {
      for(int k = 0; k < 2; k++) {
        Eigen::Vector3d weight_v(u-i, v-j, w-k);
        accum += (i*uu + (1-i)*(1-uu)) *
                 (j*vv + (1-j)*(1-vv)) *
                 (k*ww + (1-k)*(1-ww)) * c[i][j][k].dot(weight_v);
      }
    }
  }
  return accum;
}

inline static Eigen::Vector3d* perlin_generate() {
  Eigen::Vector3d * p = new Eigen::Vector3d[256];
  for(int i = 0; i < 256; ++i) {
    p[i] = Eigen::Vector3d(-1 + 2*RandomHelper::randomSeededUniform(),-1 + 2*RandomHelper::randomSeededUniform(),-1 + 2*RandomHelper::randomSeededUniform()).normalized();
  }
  return p;
}

inline void permute(int *p, int n) {
  for(int i = n-1; i > 0; --i) {
    int target = int(RandomHelper::randomSeededUniform()*(i+1));
    int tmp = p[i];
    p[i] = p[target];
    p[target] = tmp;
  }
  return;
}

inline static int* perlin_generate_perm() {
  int * p = new int[256];
  for (int i = 0; i < 256; ++i) {
    p[i] = i;
  }
  permute(p, 256);
  return p;
}

class Perlin {
public:
  Eigen::Vector3d * ranfloat;
  int *perm_x;
  int *perm_y;
  int *perm_z;

  Perlin() {
    ranfloat = perlin_generate();
    perm_x = perlin_generate_perm();
    perm_y = perlin_generate_perm();
    perm_z = perlin_generate_perm();
  }

  double noise(const Eigen::Vector3d& p) const {
    double u = p(0) - std::floor(p(0));
    double v = p(1) - std::floor(p(1));
    double w = p(2) - std::floor(p(2));

    int i = std::floor(p(0));
    int j = std::floor(p(1));
    int k = std::floor(p(2));

    Eigen::Vector3d c[2][2][2];
    for(int di = 0; di < 2; di++) {
      for(int dj = 0; dj < 2; dj++) {
        for(int dk = 0; dk < 2; dk++) {
          c[di][dj][dk] = ranfloat[perm_x[(i+di & 255)] ^ perm_y[(j + dj) & 255] ^ perm_z[(k + dk) & 255]];
        }
      }
    }

    return trilinear_interp(c, u, v, w);
  }

  double turb(const Eigen::Vector3d& p, int depth=7) const {
    double accum = 0;
    Eigen::Vector3d temp_p = p;
    double weight = 1.0;
    for (int i = 0; i < depth; ++i) {
      accum += weight*noise(temp_p);
      weight *= 0.5;
      temp_p *= 2;
    }
    return std::abs(accum);
  }
};
