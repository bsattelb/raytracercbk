#pragma once

#include <eigen3/Eigen/Dense>
#include <vector>

#include "Hitable.h"

struct Constraint {
  double equilibrium;
  double k;
  Moveable * obj1;
  Moveable * obj2;
  Constraint(double k, double e, Moveable* o1, Moveable* o2) : equilibrium(e), obj1(o1), obj2(o2), k(k) {}

  void updateAccelerations() {
    double dist = (obj1->getLoc() - obj2->getLoc()).norm();
    Eigen::Vector3d force = k*(dist - equilibrium)*(obj2->getLoc() - obj1->getLoc())/dist;
    obj1->acceleration += force/obj1->mass;
    obj2->acceleration += -force/obj2->mass;
  }
};


class MoveableList {
public:
  std::vector<Moveable *> movers;
  Bounds bound;
  std::vector<Constraint> constraints;
  Eigen::Vector3d gravity;

  MoveableList() {}
  MoveableList(std::vector<Moveable*> m) : movers(m) {gravity=Eigen::Vector3d(0, 0, 0);}
  MoveableList(std::vector<Moveable*> m, Bounds b) : movers(m), bound(b) {gravity=Eigen::Vector3d(0, 0, 0);}
  MoveableList(std::vector<Moveable*> m, Bounds b, std::vector<Constraint> c) : movers(m), bound(b), constraints(c) {gravity=Eigen::Vector3d(0, 0, 0);}
  MoveableList(std::vector<Moveable*> m, Bounds b, std::vector<Constraint> c, Eigen::Vector3d g) : movers(m), bound(b), constraints(c), gravity(g) {}

  void update(double dt) {
    for(int i = 0; i < movers.size(); ++i) {
      movers[i]->updatePosition(dt, bound);
      movers[i]->setOldAcceleration();
    }
    for(int i = 0; i < constraints.size(); ++i) {
      constraints[i].updateAccelerations();
    }
    for(int i = 0; i < movers.size(); ++i) {
      movers[i]->acceleration += dt*gravity;
    }
    for(int i = 0; i < movers.size(); ++i) {
      movers[i]->updateVelocity(dt);
    }

  }
};
