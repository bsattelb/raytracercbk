#pragma once

#include <vector>
#include <eigen3/Eigen/Dense>
#include <math.h>

#include "Sphere.h"
#include "Hitable.h"
#include "HitableList.h"
#include "RandomHelper.h"
#include "Material.h"
#include "Texture.h"
#include "Rectangle.h"

std::vector<Sphere*> makeA(Eigen::Vector3d topLeft, double width, double height, double sphereRadius, int numBalls=10, Material * mat = NULL) {
  std::vector<Sphere*> sphereList;

  double xStep = width/numBalls/2;
  double yStep = height/numBalls;

  double z = topLeft(2);

  for(int i = 0; i <= numBalls; ++i) {
    // Left
    double x = topLeft(0) - i*xStep;
    double y = topLeft(1) - height + i*yStep;
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
    // Right
    x = topLeft(0) - width + i*xStep;
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }
  // Center
  sphereList.push_back(new Sphere(Eigen::Vector3d(topLeft(0)-width/2, topLeft(1)-height/2, z), 1.25*sphereRadius, mat));
  return sphereList;
}

std::vector<Sphere*> makeB(Eigen::Vector3d topLeft, double width, double height, double sphereRadius, int numBalls=10, Material * mat = NULL) {
  std::vector<Sphere*> sphereList;

  double xStep = height/numBalls;
  double z = topLeft(2);

  // Vertical portion
  for(int i = 0; i <= numBalls; ++i) {
    double x = topLeft(0);
    double y = topLeft(1) - i*xStep;
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }

  double xRadius = width;
  double yRadius = height/4;
  double centerX = topLeft(0);
  double centerY = topLeft(1) - yRadius;

  for(double i = 1; i < numBalls; ++i) {
    // Top half
    double x = centerX + xRadius*std::cos(i*(PI)/numBalls + PI/2);
    double y = centerY + yRadius*std::sin(i*(PI)/numBalls + PI/2);
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));

    // Bottom half
    y = y - 2*yRadius;
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }

  return sphereList;
}

std::vector<Sphere*> makeC(Eigen::Vector3d topLeft, double width, double height, double sphereRadius, int numBalls=10, Material * mat = NULL) {
  std::vector<Sphere*> sphereList;

  double xCenter = topLeft(0) - width;
  double yCenter = topLeft(1) - height/2;

  double xRadius = width;
  double yRadius = height/2;

  double z = topLeft(2);

  for(double i = 0; i <= numBalls; ++i) {
    double x = xCenter + xRadius*std::cos(i*(PI)/numBalls - PI/2);
    double y = yCenter + yRadius*std::sin(i*(PI)/numBalls - PI/2);
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }
  return sphereList;
}

std::vector<Sphere*> makeD(Eigen::Vector3d topLeft, double width, double height, double sphereRadius, int numBalls=10, Material * mat = NULL) {
  std::vector<Sphere*> sphereList;

  double xStep = height/numBalls;
  double z = topLeft(2);

  // Vertical portion
  for(int i = 0; i <= numBalls; ++i) {
    double x = topLeft(0);
    double y = topLeft(1) - i*xStep;
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }

  double xCenter = topLeft(0);
  double yCenter = topLeft(1) - height/2;

  double xRadius = width;
  double yRadius = height/2;

  // Circle portion
  // Note that starting at one and going to less than numBalls
  // is so that multiple balls aren't on the corners
  for(double i = 1; i < numBalls; ++i) {
    double x = xCenter + xRadius*std::cos(i*(PI)/numBalls + PI/2);
    double y = yCenter + yRadius*std::sin(i*(PI)/numBalls + PI/2);
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }
  return sphereList;
}

std::vector<Sphere*> makeE(Eigen::Vector3d topLeft, double width, double height, double sphereRadius, int numBalls=10, Material * mat = NULL) {
  std::vector<Sphere*> sphereList;

  double yStep = height/numBalls;
  double z = topLeft(2);
  // Vertical portion
  for(int i = 0; i <= numBalls; ++i) {
    double y = topLeft(1) - i*yStep;
    sphereList.push_back(new Sphere(Eigen::Vector3d(topLeft(0), y, z), sphereRadius, mat));
  }

  // Horizontal portions
  double xStep = width/numBalls;
  for(int i = 1; i <= numBalls; ++i) {
    double x = topLeft(0) - i*xStep;
    double y = topLeft(1);
    // Top
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
    y = topLeft(1) - height/2;
    // Middle
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
    y = topLeft(1) - height;
    // Bottom
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }
  return sphereList;
}

std::vector<Sphere*> makeI(Eigen::Vector3d topLeft, double width, double height, double sphereRadius, int numBalls=10, Material * mat = NULL) {

  std::vector<Sphere*> sphereList;

  double xCenter = topLeft(0) - width/2;
  double yStep = height/numBalls;
  double z = topLeft(2);

  // Vertical
  for(int i = 1; i < numBalls; ++i) {
    double y = topLeft(1) - i*yStep;
    sphereList.push_back(new Sphere(Eigen::Vector3d(xCenter, y, z), sphereRadius, mat));
  }

  double xStep = width/numBalls;
  for(int i = 0; i <= numBalls; ++i) {
    double x = topLeft(0) - i*xStep;
    // Top
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, topLeft(1), z), sphereRadius, mat));
    // Bottom
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, topLeft(1) - height, z), sphereRadius, mat));
  }

  return sphereList;
}

std::vector<Sphere*> makeK(Eigen::Vector3d topLeft, double width, double height, double sphereRadius, int numBalls=10, Material * mat = NULL) {
  std::vector<Sphere*> sphereList;

  double xStep = height/numBalls;
  double z = topLeft(2);

  // Vertical portion
  for(int i = 0; i <= numBalls; ++i) {
    double x = topLeft(0);
    double y = topLeft(1) - i*xStep;
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }

  double xRadius = width;
  double yRadius = height/4;
  double centerX = topLeft(0);
  double centerY = topLeft(1) - yRadius;

  for(double i = 1; i < numBalls; ++i) {
    // Top half
    double x = centerX + xRadius*std::cos(i*(PI/2)/numBalls + PI/2);
    double y = centerY + yRadius*std::sin(i*(PI/2)/numBalls + 3*PI/2);
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));

    // Bottom half
    y = centerY - 2*yRadius + yRadius*std::sin(i*(PI/2)/numBalls + PI/2);
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }

  return sphereList;
}

std::vector<Sphere*> makeN(Eigen::Vector3d topLeft, double width, double height, double sphereRadius, int numBalls=10, Material * mat = NULL) {
  std::vector<Sphere*> sphereList;

  double xStep = height/numBalls;
  double z = topLeft(2);

  // Vertical portion
  for(int i = 0; i <= numBalls; ++i) {
    double x = topLeft(0);
    double y = topLeft(1) - i*xStep;
    // Left
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));

    // Right
    x = topLeft(0) - width;
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }

  xStep = width/numBalls;
  double yStep = height/numBalls;

  // Diagonal
  for(int i = 1; i < numBalls; ++i) {
    double x = topLeft(0) - width + i*xStep;
    double y = topLeft(1) - height + i*yStep;
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }
  return sphereList;
}

std::vector<Sphere*> makeS(Eigen::Vector3d topLeft, double width, double height, double sphereRadius, int numBalls=10, Material * mat = NULL) {
  std::vector<Sphere*> sphereList;

  double xRadius = width/2;
  double yRadius = height/4;
  double centerX = topLeft(0) - xRadius;
  double centerY = topLeft(1) - yRadius;

  double z = topLeft(2);

  for(double i = 0; i <= numBalls; ++i) {
    // Top half
    double x = centerX + xRadius*std::cos(i*(5*PI/4)/numBalls - PI/2);
    double y = centerY + yRadius*std::sin(i*(5*PI/4)/numBalls - PI/2);
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));

    // Bottom half
    x = centerX + xRadius*std::cos(i*(5*PI/4)/numBalls + PI/2);
    y = centerY - 2*yRadius + yRadius*std::sin(i*(5*PI/4)/numBalls + PI/2);
    sphereList.push_back(new Sphere(Eigen::Vector3d(x, y, z), sphereRadius, mat));
  }

  return sphereList;
}
