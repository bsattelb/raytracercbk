#pragma once

#include <vector>
#include "Hitable.h"
#include "BoundingBox.h"

class HitableList: public Hitable {
public:
  std::vector<Hitable*> list;

  HitableList() {}
  HitableList(std::vector<Hitable*> l) {
    list = l;
  }

  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const override;
  virtual bool bounding_box(BoundingBox& box) const override;
  virtual Ray random_ray_from_surface() const override;

  virtual double pdf_value(const Eigen::Vector3d& origin, const Eigen::Vector3d v) const override;
  virtual Eigen::Vector3d random(const Eigen::Vector3d& origin) const override;

  virtual void reset() override {
    for(int i =0; i < list.size(); ++i) {
      list[i]->reset();
    }
  }
};
