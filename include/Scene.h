#pragma once

#include "Image.h"
#include <string>

void setUpScene(int width, int height, int numSamples, int timeout, int numThreads, std::string filename);
