#pragma once
#include <eigen3/Eigen/Dense>
#include <vector>

#include "Hitable.h"
#include "BoundingBox.h"



class BVHNode: public Hitable {
public:
  std::vector<Hitable*> l;
  Hitable *left;
  Hitable *right;
  BoundingBox box;

  BVHNode() {}
  BVHNode(std::vector<Hitable*> l);

  void sort();

  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const override;
  virtual bool bounding_box(BoundingBox& box) const override;
  virtual Ray random_ray_from_surface() const override;
  virtual void reset() override;
};
