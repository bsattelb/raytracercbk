#include <fstream>
#include <eigen3/Eigen/Dense>
#include <opencv2/core/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <utility>
#include <vector>
#include <omp.h>
#include <algorithm>
#include <random>
#include <ctime>

#include <iostream>
#include <limits>

#include "Image.h"
#include "Ray.h"
#include "Sphere.h"
#include "Hitable.h"
#include "HitableList.h"
#include "RandomHelper.h"
#include "Material.h"
#include "BVHNode.h"
#include "Texture.h"
#include "Rectangle.h"
#include "MetaBall.h"
#include "PDF.h"

Image::Image(int width, int height, int numSamples, int timeout, int numThreads) :
    width(width), height(height), numSamples(numSamples), timeout(timeout),
    numThreads(numThreads){
  maxDepth = 50;
  cv::Mat temp(height, width, CV_64FC3, cv::Scalar(0, 0, 0));
  image = temp;
}
void Image::resetColor() {
  cv::Mat temp(height, width, CV_64FC3, cv::Scalar(0, 0, 0));
  image = temp;
}

Eigen::Vector3d Image::raytrace(Ray r) {
  Eigen::Vector3d albedo(1,1,1);
  return color(r, 0, albedo);
}

Eigen::Vector3d Image::color(Ray r, int depth, Eigen::Vector3d& albedo) {
  HitRecord h;
  if(world->hit(r, 0.001, std::numeric_limits<double>::max(), h)) {
    Ray scattered;
    double pdf = 1;
    Eigen::Vector3d attenuation;
    Eigen::Vector3d emitted = h.matPtr->emitted(h.u, h.v, h, r);
    if(depth <= maxDepth and h.matPtr->scatter(r, h, attenuation, scattered)) {
      albedo = albedo.cwiseProduct(attenuation);

      scattered.insideRefractive = r.insideRefractive;
      return albedo.cwiseProduct(emitted) + color(scattered, depth+1, albedo);
    } else {
      return albedo.cwiseProduct(emitted);
    }
  }
  return albedo.cwiseProduct(backgroundColor(r));
}

void Image::colorPixels() {
  Eigen::Vector3d color(0,0,0);

  std::vector<std::pair<int, int>> pixels;
  char percentDone[100] = "";

  auto start_time = time(nullptr);

  char keyPress = -1;
  int s;
  for(s = 0; s < numSamples && (timeout < 0 || time(nullptr) - start_time < timeout); s++) {
    pixels = RandomHelper::randomizePixels(width, height);

    #pragma omp parallel for private(color) shared(keyPress) num_threads(numThreads)
    for(int i = 0; i < pixels.size(); ++i) {
      std::pair<int, int> pixel = pixels[i];
      int i = pixel.first;
      int j = pixel.second;

      double u = double(i + RandomHelper::randomUniform())/double(width);
      double v = double(j + RandomHelper::randomUniform())/double(height);

      Ray r = cam.getPixelRay(u, v);
      color = raytrace(r);


      bool isNans = false;
      for(int i = 0; i < 3; ++i) {
        if(isnan(color(i))) {
          color(i) = 0;
          isNans = true;
        }
        if(color(i) > 1) {
          color(i) = 1;
        }
        if(color(i) < 0) {
          color(i) = 0;
         }
      }

      // OpenCV is BGR
      color.reverseInPlace();
      cv::Vec<double, 3> cvColor;
      cv::eigen2cv(color, cvColor);
      image.at<cv::Vec<double, 3>>(cv::Point(i, height - 1 - j)) += cvColor;
    }
  }
  image = image/double(s);
  cv::sqrt(image, image);
  std::cout << s << " samples in " << time(nullptr) - start_time << " seconds" << std::endl;
}

void Image::writeToFile(std::string filename) {
  image.convertTo(image, CV_8UC3, 255.0);
  cv::imwrite(filename, image);
}
