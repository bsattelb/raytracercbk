#include <eigen3/Eigen/Dense>
#include <iostream>
#include <omp.h>

#include "Scene.h"

int main(int argc, char* argv[]) {
  if(argc < 2) {
    std::cout << "Not enough file names entered.  Exiting.";
    return -1;
  }

  int width = 480;
  int height = 240;
  int numSamples = 100;
  int timeout = -1;
  int numThreads = 1;

  for (int i = 2; i < argc; ++i) {
    if (std::string(argv[i]) == "--resolution") {
      if (i + 1 < argc) {
        width = std::stoi(argv[++i]);
        height = std::stoi(argv[++i]);
      } else {
        std::cerr << "--resolution option requires two arguments." << std::endl;
        return 1;
      }
    } else if (std::string(argv[i]) == "--samples") {
      if (i + 1 < argc) {
        numSamples = std::stoi(argv[++i]);
      } else {
        std::cerr << "--samples option requires one argument." << std::endl;
        return 1;
      }
    } else if (std::string(argv[i]) == "--timeout") {
      if (i + 1 < argc) {
        timeout = std::stoi(argv[++i]);
      } else {
        std::cerr << "--timeout option requires one argument." << std::endl;
        return 1;
      }
    } else if (std::string(argv[i]) == "--threads") {
      if (i + 1 < argc) {
        numThreads = std::stoi(argv[++i]);
      } else {
        std::cerr << "--threads option requires one argument." << std::endl;
        return 1;
      }
    }
  }

  // Image image(width, height, numSamples, timeout, numThreads);
  // image.colorPixels();
  // image.writeToFile(argv[1]);
  setUpScene(width, height, numSamples, timeout, numThreads, argv[1]);

  return 0;
}
