#include <random>
#include <vector>
#include <utility>
#include "RandomHelper.h"

std::random_device rd;
std::default_random_engine RandomHelper::rng(rd());

std::default_random_engine RandomHelper::seededRNG(1337);
