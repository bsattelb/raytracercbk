#include "Scene.h"

#include <fstream>
#include <eigen3/Eigen/Dense>
#include <utility>
#include <vector>
#include <algorithm>
#include <random>
#include <ctime>
#include <math.h>

#include <iostream>
#include <limits>
#include <string>

#include "Image.h"
#include "Ray.h"
#include "Sphere.h"
#include "Hitable.h"
#include "HitableList.h"
#include "RandomHelper.h"
#include "Material.h"
#include "Texture.h"
#include "Rectangle.h"
#include "MetaBall.h"
#include "PDF.h"
#include "ConstantMedium.h"
#include "Alphabet.h"
#include "Moveable.h"
#include "MoveableList.h"

MoveableList benScene(Image &image, int width, int height) {
  std::vector<Hitable*> list;

  std::vector<Hitable*> lightList;
  Material *glass = new Dielectric(2, Eigen::Vector3d(0.99, 0.95, 0.99));
  Material *metal = new Metal(new ConstantTexture(Eigen::Vector3d(0.9, 0.9, 0.9)), 0.05);
  Material *light = new DiffuseLight(new ConstantTexture(Eigen::Vector3d(15, 15, 15)));

  Hitable *light1 = new xz_rect(-100, 100, -100, 100, 299, light);

  lightList.push_back(light1);
  list.insert(list.end(), lightList.begin(), lightList.end());
  image.lights = new HitableList(lightList);

  double mix = 0.5; // 0 is only direct, 1 is only indirect
  Material *red = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.65, 0.06, 0.06)), image.lights, mix);
  Material *white = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.73, 0.73, 0.73)), image.lights, mix);
  Material *green = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.12, 0.45, 0.15)), image.lights, mix);
  Material *blue = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.12, 0.12, 0.73)), image.lights, mix);
  Material *yellow = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.73, 0.73, 0.12)), image.lights, mix);
  Material *checkerBoard = new Lambertian(new CheckerTexture(new ConstantTexture(Eigen::Vector3d(0.73, 0.73, 0.73)), new ConstantTexture(Eigen::Vector3d(0.12, 0.12, 0.12)), 0.05), image.lights, mix);
  Material *checkerBoard2 = new Lambertian(new CheckerTexture(new ConstantTexture(Eigen::Vector3d(0, 0.73, 0.73)), new ConstantTexture(Eigen::Vector3d(1, 0, 1)), 0.05), image.lights, mix);

  list.push_back(new yz_rect(-300, 300, -500, 300, 600, green));
  list.push_back(new yz_rect(-300, 300, -500, 300, -600, red));
  list.push_back(new xz_rect(-600, 600, -500, 300, 300, blue));
  list.push_back(new xz_rect(-600, 600, -500, 300, -300, checkerBoard));

  list.push_back(new xy_rect(-600, 600, -300, 300, 300, checkerBoard2));
  list.push_back(new uni_xy_rect(-600, 600, -300, 300, -500, checkerBoard, 1));

  Eigen::Vector3d lookfrom(0,0,-1200);
  Eigen::Vector3d lookat(0,0,0);
  image.cam.setUp(lookfrom, lookat, Eigen::Vector3d(0,1,0), 40, double(width)/double(height), 0.0, 1.0);

  //B
  std::vector<Sphere*> metaList = makeB(Eigen::Vector3d(400, 150, 0), 150, 300, 50, 50);

  //E
  std::vector<Sphere*> metaList2 = makeE(Eigen::Vector3d(75, 150, 0), 150, 300, 50, 50);

  //N
  std::vector<Sphere*> metaList3 = makeN(Eigen::Vector3d(-250, 150, 0), 150, 300, 50, 50);

  list.push_back(new MetaBall(metaList, glass));
  list.push_back(new MetaBall(metaList2, white));
  list.push_back(new MetaBall(metaList3, metal));

  std::vector<Moveable*> movers;
  for(int i = 0; i < metaList.size(); ++i) {
    movers.push_back(new Moveable(metaList[i], RandomHelper::randomInUnitSphere()));
  }
  std::vector<Constraint> constraints;
  for(int i = 0; i < movers.size(); ++i) {
    for(int j = i; j < movers.size(); ++j) {
      if(i != j) {
        constraints.push_back(Constraint(0.001, (movers[i]->getLoc() - movers[j]->getLoc()).norm(), movers[i], movers[j]));
      }
    }
  }
  for(int i = 0; i < metaList2.size(); ++i) {
    movers.push_back(new Moveable(metaList2[i], RandomHelper::randomInUnitSphere()));
  }
  for(int i = metaList.size(); i < movers.size(); ++i) {
    for(int j = i; j < movers.size(); ++j) {
      if(i != j) {
        constraints.push_back(Constraint(0.01, (movers[i]->getLoc() - movers[j]->getLoc()).norm(), movers[i], movers[j]));
      }
    }
  }
  for(int i = 0; i < metaList3.size(); ++i) {
    movers.push_back(new Moveable(metaList3[i], RandomHelper::randomInUnitSphere()));
  }
  for(int i = metaList.size()+metaList2.size(); i < movers.size(); ++i) {
    for(int j = i; j < movers.size(); ++j) {
      if(i != j) {
        constraints.push_back(Constraint(0.1, (movers[i]->getLoc() - movers[j]->getLoc()).norm(), movers[i], movers[j]));
      }
    }
  }

  image.world = new HitableList(list);

  Bounds b;
  b.min = Eigen::Vector3d(-600, -300, -300);
  b.max = Eigen::Vector3d(600, 300, 300);
  return MoveableList(movers, b, constraints, Eigen::Vector3d(0, -9.8, 0));
}

MoveableList testScene(Image &image, int width, int height) {
  std::vector<Hitable*> list;

  std::vector<Hitable*> lightList;
  Material *metal = new Metal(new ConstantTexture(Eigen::Vector3d(0.5, 0.5, 0.5)), 0.05);

  Eigen::Vector3d lookfrom(0,0,-1200);
  Eigen::Vector3d lookat(0,0,0);
  image.cam.setUp(lookfrom, lookat, Eigen::Vector3d(0,1,0), 40, double(width)/double(height), 0.0, 1.0);

  image.lights = new HitableList(lightList);

  std::vector<Sphere*> spheres;
  std::vector<Moveable*> movers;

  spheres.push_back(new Sphere(Eigen::Vector3d(100, 0, 0) , 50, metal));
  movers.push_back(new Moveable(spheres[0], Eigen::Vector3d(1, 0, 0)));
  list.push_back(spheres[0]);

  spheres.push_back(new Sphere(Eigen::Vector3d(-100, 0, 0) , 50, metal));
  movers.push_back(new Moveable(spheres[1], Eigen::Vector3d(-1, 0, 0)));
  list.push_back(spheres[1]);

  spheres.push_back(new Sphere(Eigen::Vector3d(0, 100, 0) , 50, metal));
  movers.push_back(new Moveable(spheres[2], Eigen::Vector3d(0, 0, 0)));
  list.push_back(spheres[2]);

  std::vector<Constraint> constraints;
  constraints.push_back(Constraint(1000, (movers[0]->getLoc() - movers[1]->getLoc()).norm(), movers[0], movers[1]));
  constraints.push_back(Constraint(1000, (movers[0]->getLoc() - movers[2]->getLoc()).norm(), movers[0], movers[2]));
  constraints.push_back(Constraint(1000, (movers[1]->getLoc() - movers[2]->getLoc()).norm(), movers[1], movers[2]));

  image.world = new HitableList(list);
  Bounds b;
  b.min = Eigen::Vector3d(-600, -300, -300);
  b.max = Eigen::Vector3d(600, 300, 300);
  return MoveableList(movers, b, constraints);
}

void setUpScene(int width, int height, int numSamples, int timeout, int numThreads, std::string filename) {
  Image image1(width, height, numSamples, timeout, numThreads);
  MoveableList movers = benScene(image1, width, height);
  for(int i = 0; i < 1000; ++i) {
    image1.resetColor();
    image1.colorPixels();
    char buffer[5];
    std::snprintf(buffer, sizeof(buffer), "%04d", i);
    std::cout << filename + buffer + ".png" << std::endl;
    image1.writeToFile(filename + buffer + ".png");

    for(int j = 0; j < 1000; ++j) {
      movers.update(0.001);
      image1.world->reset();
    }
  }
}
