#include <vector>
#include <eigen3/Eigen/Dense>
#include <algorithm>
#include <stdlib.h>

#include <iostream>

#include "MetaBall.h"
#include "BoundingBox.h"

// Check if we hit any of the included spheres
bool MetaBall::checkContainerHits(const Ray& r, double t_min, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const {
  bool hitAnything = false;
  for(int i = 0; i < balls.size(); ++i) {
    Eigen::Vector2d tVals;
    bool didCollide = balls[i]->getTvals(r, tVals);
    hitLocations[i] = std::tuple<bool, double, double>(didCollide, tVals[0], tVals[1]);
    hitAnything = hitAnything or didCollide;
  }
  return hitAnything;
}

// Calculate the potential field at some point
double MetaBall::checkPotential(const Eigen::Vector3d& point, double t, double t_max, std::vector<std::tuple<bool, double, double>>& hitLocations) const {
  double sum = 0;
  double d = 0;

  bool inSphere;
  double sphereStart;
  double sphereEnd;

  for(int i = 0; i < balls.size(); ++i) {
    std::tie(inSphere, sphereStart, sphereEnd) = hitLocations[i];
    if(inSphere && sphereStart < t && t < sphereEnd) {
      d = (point - balls[i]->center).norm()/balls[i]->radius;
      sum += 1 - d*d*d*(d*(d*6 - 15) + 10);
    }
  }
  return sum;
}

double MetaBall::checkPotential(const Eigen::Vector3d& point) const {
  double sum = 0;
  double d = 0;

  for(int i = 0; i < balls.size(); ++i) {
    d = (point - balls[i]->center).norm()/balls[i]->radius;
    if(0 < d && d < 1) {
      sum += 1 - d*d*d*(d*(d*6 - 15) + 10);
    }
  }
  return sum;
}

// Calculate the surface normal at some point
Eigen::Vector3d MetaBall::calcNormal(Eigen::Vector3d& point, double t, std::vector<std::tuple<bool, double, double>>& hitLocations) const {
  Eigen::Vector3d sum(0,0,0);
  double d = 0;

  bool inSphere;
  double sphereStart;
  double sphereEnd;

  for(int i = 0; i < balls.size(); ++i) {
    std::tie(inSphere, sphereStart, sphereEnd) = hitLocations[i];
    if(inSphere && sphereStart < t && t < sphereEnd) {
      Eigen::Vector3d N = (point-balls[i]->center);
      d = (point - balls[i]->center).norm()/balls[i]->radius;
      sum += 6*d*d*(d*(5*d - 10) + 5)*N/(N.norm()*balls[i]->radius);
    }
  }
  return sum.normalized();
}


// Calculate the smallest t_value to start ray marching at
double getSmallestT(std::vector<std::tuple<bool, double, double>> hitLocations, double t_min, double t_max) {
  bool didCollide;
  double sphereStart;
  double sphereEnd;

  double minActualT = t_max;

  for(int i = 0; i < hitLocations.size(); ++i) {
    std::tie(didCollide, sphereStart, sphereEnd) = hitLocations[i];

    // If we are inside this sphere we need to start where we are
    if(didCollide && sphereStart < t_min && t_min < t_max) {
      return t_min;
    }
    // Otherwise, get the first sphere we intersect
    if(didCollide && t_min < sphereStart && sphereStart < minActualT) {
      minActualT = sphereStart;
    }
  }
  return minActualT;
}

// Calculate the maximum t_value to end ray marching at
double getLargestT(std::vector<std::tuple<bool, double, double>> hitLocations, double t_min, double t_max) {
  bool didCollide;
  double sphereStart;
  double sphereEnd;

  double maxActualT = t_min;

  for(int i = 0; i < hitLocations.size(); ++i) {
    std::tie(didCollide, sphereStart, sphereEnd) = hitLocations[i];

    // Check the sphere's exit - we don't have to worry about the start
    if(didCollide && maxActualT < sphereEnd && sphereEnd < t_max) {
      maxActualT = sphereEnd;
    }
  }
  return maxActualT;
}


bool MetaBall::hit(const Ray& r, double t_min, double t_max, HitRecord& h) const {
  if(!box.hit(r, t_min, t_max)) {
    return false;
  }

  std::vector<std::tuple<bool, double, double>> hitLocations(balls.size());
  bool didCollide = checkContainerHits(r, t_min, t_max, hitLocations);

  if(!didCollide) {
    return false;
  }

  double minT = getSmallestT(hitLocations, t_min, t_max);
  double maxT = getLargestT(hitLocations, t_min, t_max);

  if(maxT <= minT) {
    return false;
  }

  double curT = minT;

  Eigen::Vector3d point = r.pointAt(curT);
  double tStep = (maxT - minT)/200;

  if(r.insideRefractive) {
    curT += tStep/100;
    point = r.pointAt(curT);

    while(curT < maxT && checkPotential(point, curT, t_max, hitLocations) > cutoff) {
      curT += tStep;
      point = r.pointAt(curT);
    }

    curT -= tStep;
    tStep = tStep/200;
    curT += tStep;


    while(curT < maxT && checkPotential(point, curT, t_max, hitLocations) > cutoff) {
      curT += tStep;
      point = r.pointAt(curT);
    }

    //std::cout << minT << " " << curT << " " << maxT <<  std::endl;
  } else {
    while(curT < maxT && checkPotential(point, curT, t_max, hitLocations) < cutoff) {
      curT += tStep;
      point = r.pointAt(curT);
    }

    curT = curT-tStep;
    tStep = tStep/200;
    curT += tStep;

    while(curT < maxT && checkPotential(point, curT, t_max, hitLocations) < cutoff) {
      curT += tStep;
      point = r.pointAt(curT);
    }
  }

  if(curT >= maxT || curT < minT) {
    return false;
  }

  h.t = curT;
  h.point = r.pointAt(curT);
  h.matPtr = matPtr;
  h.N = calcNormal(h.point, h.t, hitLocations);
  return true;
}

bool MetaBall::bounding_box(BoundingBox& box) const {
  box = this->box;
  return true;
}

Ray MetaBall::random_ray_from_surface() const {
  return Ray(); // NOT IMPLEMENTED
}
